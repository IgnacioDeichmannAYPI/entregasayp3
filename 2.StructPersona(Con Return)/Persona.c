#include "PersonaEjercicio/Persona.h"

int main()
{
    Persona unaPersona;
    unaPersona = inicializar(unaPersona);

    unaPersona = cambiarDni(unaPersona, 1111111);
    unaPersona = cambiarEdad(unaPersona, 45);
    unaPersona = establecerNombre(unaPersona, "Batman");
    unaPersona = establecerApellido(unaPersona, "Lopez");
    unaPersona = establecerCorreoElectronico(unaPersona, "batmanlopez@ejemplo");

    Persona otraPersona;
    otraPersona = inicializar(otraPersona);

    otraPersona = cambiarDni(otraPersona, 222222);
    otraPersona = cambiarEdad(otraPersona, 75);
    otraPersona = establecerNombre(otraPersona, "Superman");
    otraPersona = establecerApellido(otraPersona, "Perez");
    otraPersona = establecerCorreoElectronico(otraPersona, "supermanperez@ejemplo");

    imprimirPersona(unaPersona);
    imprimirPersona(otraPersona);

    return 0;
}

/**
 * Interpreto que al pasar los structs como parametros en las funciones.
 * Se genera una copia, como si de tipos primitivos se tratase.
 *
 * Por lo tanto, no modificamos el struct original, sino una copia en memoria.
 *
 * Por eso debemos retornar su valor, si queremos utilizarlo.
 */