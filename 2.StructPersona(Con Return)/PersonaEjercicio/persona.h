#include <stdio.h>
#include <string.h>

typedef struct Persona Persona;
/**
 * @brief               La estructura modela una Persona.
 */
struct Persona
{
    int dni;
    int edad;
    char *nombre;
    char *apellido;
    char *correoElectronico;
};

/**
 * @brief               Inicializa la persona para que no guarde basura.
 *
 * @param persona:      Estructura persona
 *
 * @return Persona:     Persona inicializada.
 */
Persona inicializar(Persona persona)
{
    persona.dni = NULL;
    persona.edad = NULL;
    persona.nombre = NULL;
    persona.apellido = NULL;
    persona.correoElectronico = NULL;
    return persona;
}

extern void imprimirPersona(Persona miPersona);
extern Persona cambiarEdad(Persona unaPersona, int edad);

/**
 * @brief               Retorna la persona con el dni pasado
 *                      como parametro.
 *
 * @param unaPersona:   Estructura persona.
 *
 * @param dni           Dni de la persona
 *
 * @return Persona      Retorna la persona con un nuevo dni.
 */
Persona cambiarDni(Persona unaPersona, int dni)
{
    unaPersona.dni = dni;
    return unaPersona;
}

/**
 * @brief               Retorna la persona con la edad pasada
 *                      como parámetro.
 *
 * @param unaPersona:   Estructura persona.
 *
 * @param edad          Edad de la persona
 *
 * @return Persona      Retorna la persona con la nueva edad.
 */
Persona cambiarEdad(Persona unaPersona, int edad)
{
    unaPersona.edad = edad;
    return unaPersona;
}

/**
 * @brief               Retorna la persona con el nombre pasado
 *                      como parametro.
 *
 * @param unaPersona:   Estructura persona.
 *
 * @param nombre:       Nombre de la persona
 *
 * @return Persona:     Retorna la persona con el nuevo nombre.
 */
Persona establecerNombre(Persona unaPersona, char *nombre)
{
    unaPersona.nombre = nombre;
    return unaPersona;
}

/**
 * @brief               Retorna la persona con el apellido pasado
 *                      como parámetro.
 *
 * @param unaPersona:   Estructura persona.
 *
 * @param edad:         Apellido de la persona
 *
 * @return Persona      Retorna la persona con el nuevo apellido.
 */
Persona establecerApellido(Persona unaPersona, char *apellido)
{
    unaPersona.apellido = apellido;
    return unaPersona;
}

/**
 * @brief                       Retorna la persona con el correo electrónico pasado
 *                              como parámetro.
 *
 * @param unaPersona:           Estructura persona.
 *
 * @param correoElectronico:    Ccorreo electrónico de la persona.
 *
 * @return Persona              Retorna la persona con el nuevo correo electrónico.
 */
Persona establecerCorreoElectronico(Persona unaPersona, char *correoElectronico)
{
    unaPersona.correoElectronico = correoElectronico;
    return unaPersona;
}

/**
 * @brief               Muestra en consola miPersona pasada como parámetro.
 *
 * @param miPersona     La persona que quiero imprimir.
 */
void imprimirPersona(Persona miPersona)
{
    printf("\nPersona");
    printf("[Dni = %d , ", miPersona.dni);
    printf("Edad = %d , ", miPersona.edad);
    printf("Nombre = %s , ", miPersona.nombre);
    printf("Apellido = %s , ", miPersona.apellido);
    printf("Correo Electronico = %s ]", miPersona.correoElectronico);
}