// Autor: Ignacio Deichmann.

#include <stdio.h>
#include <malloc.h>

/**
 * @brief                          La estructura modela un Nodo de una Lista Simplemente Enlazada.
 */
typedef struct structNodo
{
    int valor;
    int largo;
    struct structNodo *siguiente;
} Nodo;

/**
 * @brief                           Crea e inicializa una lista.
 *
 * @param lista:                    Direccion de memoria de la lista a inicializar.
 */
void inicializarlista(Nodo **lista)
{
    *lista = NULL;
}

/**
 * @brief                           Agrega un valor a la lista pasada como parámetro.
 *
 * @param lista                     Direccion de memoria del Nodo principal de la lista ("Header").
 *
 * @param valor:                    Valor que queremos agregar en la lista.
 *
 */
void agregarElemento(Nodo **lista, int valor)
{
    Nodo *NodoNuevo = (Nodo *)malloc(sizeof(Nodo));
    NodoNuevo->valor = valor;
    NodoNuevo->siguiente = NULL;
    if (*lista == NULL)
    {
        *lista = NodoNuevo;
    }
    else
    {
        Nodo *cursor = *lista;
        while (cursor->siguiente != NULL)
        {
            cursor = cursor->siguiente;
        }
        cursor->siguiente = NodoNuevo;
    }
    Nodo *listaAuxiliar = malloc(sizeof(Nodo));
    listaAuxiliar = *lista;
    listaAuxiliar->largo = listaAuxiliar->largo + 1;
}

/**
 * @brief                           Devuelve el largo de la lista.
 *
 * @param lista                     Direccion de memoria del Nodo principal de la lista ("Header").
 *
 * @return int:                     La cantidad de elementos de la lista.
 */
int obtenerLargoLista(Nodo *lista)
{
    if (lista == NULL)
        return 0;
    return lista->largo;
}

/**
 * @brief                           Imprime la lista por consola.
 *
 * @param lista                     Direccion de memoria del Nodo principal de la lista ("Header").
 */
void imprimirLista(Nodo *lista)
{
    Nodo *nodoAImprimir = lista;

    do
    {
        printf("%d \n", nodoAImprimir->valor);
        nodoAImprimir = nodoAImprimir->siguiente;
    } while (nodoAImprimir != NULL);
}

/**
 * @brief                           Dado un índice, devuelve el elemento de la
 *                                  lista en la posición solicitada.
 *
 * @param lista                     Direccion de memoria del Nodo principal de la lista ("Header").
 *
 * @param indiceDePosicion:         Indice, elemento N al que quiere acceder,
 *                                  contando desde la posición desde el 0.
 *
 * @return int                      El resultado de obtener un elemento N de la lista.
 */
int obtenerElementoN(Nodo **lista, int indiceDePosicion)
{
    if (indiceDePosicion < 0 || indiceDePosicion > obtenerLargoLista(*lista) - 1)
        return NULL; // "%s" Le pone 0 en vez de null al int al imprimir.
    Nodo *nodoElemento = malloc(sizeof(Nodo));
    nodoElemento = *lista;
    for (int i = 0; i < indiceDePosicion; i++)
    {
        nodoElemento = nodoElemento->siguiente;
    }
    return nodoElemento->valor;
}

/**
 * @brief                           Elimina un elemento N de la lista.
 *
 * @param lista                     Direccion de memoria del Nodo principal de la lista ("Header").
 *
 * @param indiceDePosicion          Índice del elemento N que se quiere eliminar de la lista.
 *
 */
void eliminarElementoN(Nodo **lista, int indiceDePosicion)
{
    Nodo *punteroNodo = malloc(sizeof(Nodo));
    punteroNodo = *lista;
    if (indiceDePosicion < 0 || indiceDePosicion > obtenerLargoLista(*lista) - 1)
    {
        printf("\nERROR: No se han eliminado elementos de la lista.\nPues el indice no pertenece a la lista.\n");
    }
    else if (indiceDePosicion == 0)
    {
        if (punteroNodo->largo > 1)
        {
            punteroNodo->siguiente->largo = punteroNodo->largo - 1;
            punteroNodo = punteroNodo->siguiente;
        }
        else
        {
            punteroNodo = NULL;
        }
    }
    else
    {
        Nodo *nodoAnterior = punteroNodo;

        int i = 0;
        while (i < indiceDePosicion - 1)
        {

            nodoAnterior = nodoAnterior->siguiente;
            i++;
        }
        printf("\nBorro el %d  \n", nodoAnterior->siguiente->valor);

        Nodo *itemABorrar = nodoAnterior->siguiente;
        Nodo *siguienteAlProximo;
        if (itemABorrar != NULL)
        {
            siguienteAlProximo = itemABorrar->siguiente;
        }
        else
        {
            siguienteAlProximo = NULL;
        }

        if (siguienteAlProximo == NULL)
        {
            nodoAnterior->siguiente = NULL;
            free(itemABorrar);
        }
        else
        {
            nodoAnterior->siguiente = siguienteAlProximo;
            free(itemABorrar);
        }

        punteroNodo->largo = punteroNodo->largo - 1;
    }
}

/**
 * @brief                           Intercambia 2 números entre variables.
 *
 * @param a:                        Puntero del numero a.
 *
 * @param b:                        Puntero del numero b.
 */
void intercambio(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

/**
 * @brief                           Agrega un elemento ordenado de menor a mayor a la lista.
 *
 * @param lista                     Direccion de memoria del Nodo principal de la lista ("Header").
 *
 * @param valorDeEntrada            Direccion de memoria de la entidad que queremos agregar.
 *
 */
void agregarElementoOrdenado(Nodo **lista, int *valorDeEntrada)
{
    if (obtenerLargoLista(*lista) > 0)
    {
        Nodo *actualValor = malloc(sizeof(Nodo));
        actualValor = *lista;

        for (int i = 0; i < obtenerLargoLista(*lista); i++)
        {
            if (actualValor->valor > valorDeEntrada)
            {
                intercambio(&(actualValor->valor), &(valorDeEntrada));
            }
            if (i != obtenerLargoLista(*lista) - 1)
                actualValor = actualValor->siguiente;
        }
    }
    agregarElemento(lista, valorDeEntrada);
}
