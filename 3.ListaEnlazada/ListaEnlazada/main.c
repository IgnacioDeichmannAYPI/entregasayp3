// Autor: Ignacio Deichmann.

#include "Source/ListaSimplementeEnlazada/ListaSimplementeEnlazada.h"
#include <assert.h>

int main()
{

    printf("\n>> ListaOrdenada\n");
    Nodo *lista;
    inicializarlista(&lista);

    agregarElementoOrdenado(&lista, 10);
    agregarElementoOrdenado(&lista, 4);
    agregarElementoOrdenado(&lista, 2);
    agregarElementoOrdenado(&lista, 8);
    agregarElementoOrdenado(&lista, 3);
    agregarElementoOrdenado(&lista, 1);
    agregarElementoOrdenado(&lista, 9);
    agregarElementoOrdenado(&lista, 7);
    agregarElementoOrdenado(&lista, 6);
    agregarElementoOrdenado(&lista, 5);

    imprimirLista(lista);
    printf(">> Largo: %d \n", obtenerLargoLista(lista));
    printf("------------------------------------------\n");

    assert(lista != NULL);
    assert(lista->valor == 1);
    assert(lista->siguiente->valor == 2);
    assert(lista->siguiente->siguiente->valor == 3);
    assert(lista->siguiente->siguiente->siguiente->valor == 4);
    assert(lista->siguiente->siguiente->siguiente->siguiente->valor == 5);
    assert(lista->siguiente->siguiente->siguiente->siguiente->siguiente->valor == 6);
    assert(lista->siguiente->siguiente->siguiente->siguiente->siguiente->siguiente->valor == 7);
    assert(lista->siguiente->siguiente->siguiente->siguiente->siguiente->siguiente->siguiente->valor == 8);
    assert(lista->siguiente->siguiente->siguiente->siguiente->siguiente->siguiente->siguiente->siguiente->valor == 9);
    assert(lista->siguiente->siguiente->siguiente->siguiente->siguiente->siguiente->siguiente->siguiente->siguiente->valor == 10);

    return 0;
}