// Autor: Ignacio Deichmann.

#include <assert.h>
#include "../../Source/ListaSimplementeEnlazada/ListaSimplementeEnlazada.h"

// Test
void deboPoderCrearUnaLista()
{
    printf(">>> Lista\n");
    Nodo *lista;
    inicializarlista(&lista);

    agregarElemento(&lista, 54);
    agregarElemento(&lista, 35);
    agregarElemento(&lista, 3);

    imprimirLista(lista);
    printf(">> Largo: %d \n", obtenerLargoLista(lista));
    printf("------------------------------------------\n");

    assert(lista != NULL);
}

// Test
void deboPoderCrearUnaListaOrdenada()
{
    printf("\n>>> ListaOrdenada\n");
    Nodo *lista;
    inicializarlista(&lista);

    agregarElementoOrdenado(&lista, 2);
    agregarElementoOrdenado(&lista, 3);
    agregarElementoOrdenado(&lista, 1);

    imprimirLista(lista);
    printf(">> Largo: %d \n", obtenerLargoLista(lista));
    printf("------------------------------------------\n");

    assert(lista != NULL);
    assert(lista->valor == 1);
    assert(lista->siguiente->valor == 2);
    assert(lista->siguiente->siguiente->valor == 3);
}

// Test
void deboPoderEliminarUnElementoDeLaLista()
{
    printf("\n>>> Lista Original\n");
    Nodo *lista;
    inicializarlista(&lista);

    agregarElementoOrdenado(&lista, 82);
    agregarElementoOrdenado(&lista, 43);
    agregarElementoOrdenado(&lista, 71);
    imprimirLista(lista);

    printf(">> Largo: %d \n", obtenerLargoLista(lista));
    assert(lista != NULL);
    assert(lista->valor == 43);
    assert(lista->siguiente->valor == 71);
    assert(lista->siguiente->siguiente->valor == 82);
    eliminarElementoN(&lista, 1); // INDICE CUENTA LA POSICION DESDE 0.
    printf("\n>> Lista Sin el elemento en la posicion 1:\n");
    imprimirLista(lista);
    printf(">> Largo: %d \n", obtenerLargoLista(lista));
    printf("------------------------------------------\n");

    assert(lista->valor == 43);
    assert(lista->siguiente->valor == 82);
}

int pruebaManual()
{

    Nodo *lista;
    inicializarlista(&lista);

    printf("Largo: %d \n", obtenerLargoLista(lista));

    agregarElementoOrdenado(&lista, 33);

    agregarElemento(&lista, 11);
    agregarElemento(&lista, 21);
    agregarElemento(&lista, 31);

    printf("----------------------\n");
    printf("Elementos de la lista:");
    printf("\n----------------------\n");
    imprimirLista(lista);
    printf("----------------------\n");

    printf("Largo: %d \n", obtenerLargoLista(lista));

    printf("lista[%d] = %d \n", -1, obtenerElementoN(&lista, -1));
    printf("lista[%d] = %d \n", 0, obtenerElementoN(&lista, 0));
    printf("lista[%d] = %d \n", 1, obtenerElementoN(&lista, 1));
    printf("lista[%d] = %d \n", 2, obtenerElementoN(&lista, 2));
    printf("lista[%d] = %d \n", 3, obtenerElementoN(&lista, 3));
    printf("lista[%d] = %d \n", 4, obtenerElementoN(&lista, 4));

    // Probando insertar ordenado
    printf("INSERTO UN VALOR:\n");
    agregarElementoOrdenado(&lista, 5);
    agregarElementoOrdenado(&lista, 109);
    agregarElementoOrdenado(&lista, 14);

    printf("Largo: %d \n", obtenerLargoLista(lista));

    printf("----------------------\n");
    printf("Elementos de la lista:");
    printf("\n----------------------\n");
    imprimirLista(lista);
    printf("----------------------\n");

    printf("lista[%d] = %d \n", -1, obtenerElementoN(&lista, -1));
    printf("lista[%d] = %d \n", 0, obtenerElementoN(&lista, 0));
    printf("lista[%d] = %d \n", 1, obtenerElementoN(&lista, 1));
    printf("lista[%d] = %d \n", 2, obtenerElementoN(&lista, 2));
    printf("lista[%d] = %d \n", 3, obtenerElementoN(&lista, 3));
    printf("lista[%d] = %d \n", 4, obtenerElementoN(&lista, 4));
    printf("lista[%d] = %d \n", 5, obtenerElementoN(&lista, 5));
    printf("lista[%d] = %d \n", 6, obtenerElementoN(&lista, 6));
    printf("null se imprimió como 0\n");
    printf("lista[%d] = %s \n", 7, obtenerElementoN(&lista, 7));

    // ELIMINANDO ITEM
    printf("Largo: %d \n", obtenerLargoLista(lista));

    printf("ELIMINO ITEM\n");
    eliminarElementoN(&lista, 3);

    printf("----------------------\n");
    printf("Elementos de la lista:");
    printf("\n----------------------\n");
    imprimirLista(lista);
    printf("----------------------\n");
    printf("Largo: %d \n", obtenerLargoLista(lista));

    //

    printf("ELIMINO ITEM\n");
    eliminarElementoN(&lista, 2);

    printf("----------------------\n");
    printf("Elementos de la lista:");
    printf("\n----------------------\n");
    imprimirLista(lista);
    printf("----------------------\n");
    printf("Largo: %d \n", obtenerLargoLista(lista));

    //

    printf("ELIMINO ITEM\n");
    eliminarElementoN(&lista, 3);

    printf("----------------------\n");
    printf("Elementos de la lista:");
    printf("\n----------------------\n");
    imprimirLista(lista);
    printf("----------------------\n");
    printf("Largo: %d \n", obtenerLargoLista(lista));

    printf("ELIMINO ITEM\n");
    eliminarElementoN(&lista, 3);

    printf("----------------------\n");
    printf("Elementos de la lista:");
    printf("\n----------------------\n");
    imprimirLista(lista);
    printf("----------------------\n");
    printf("Largo: %d \n", obtenerLargoLista(lista));

    return 0;
}

int main()
{
    deboPoderCrearUnaLista();
    deboPoderCrearUnaListaOrdenada();
    deboPoderEliminarUnElementoDeLaLista();
    // pruebaManual();

    return 0;
}
