#include "Persona/persona.h"

int main()
{
    // Ingresar datos en struct.

    Persona humano1;
    inicializarPersona(&humano1);

    Persona humano2;
    inicializarPersona(&humano2);

    imprimirPersona(&humano1);
    imprimirPersona(&humano2);

    // Cambiar datos.

    Persona humano3;
    establecerDni(&humano3, 111111);
    establecerEdad(&humano3, 34);
    establecerNombre(&humano3, "El nombre que quiero");
    establecerApellido(&humano3, "El apellido que quiero");
    establecerCorreoElectronico(&humano3, "correo@ejemplo");
    imprimirPersona(&humano3);

    return 0;
}

