#include <stdio.h>
#include <string.h>

/**
 * @brief               La estructura modela una Persona.
 */
typedef struct
{
    int dni;
    int edad;
    char *nombre;
    char *apellido;
    char *correoElectronico;
} Persona;

/**
 * @brief               Devuelve una copia de la cadena.
 *
 * @param cadena        Cadena de entrada.
 *
 * @return char*        Retorna una copia de la cadena.
 */
char *copiarCadena(char *cadena)
{
    char *aux = malloc(sizeof(char) * strlen(cadena) + 1);
    for (int i = 0; i < strlen(cadena); i++)
        aux[i] = cadena[i];
    return aux;
}

/**
 * @brief               Inicializa la Persona, solicitando informacion,
 *                      para evitar que hayan campos apuntando a direcciones
 *                      de memoria aleatorias, con datos irrelevantes.
 *
 * @param miPersona     La direccion de memoria de la persona que
 *                      queremos inicializar.
 */
void inicializarPersona(Persona *miPersona)
{
    printf("\nIngresar Persona:\n");

    int auxint[2];
    printf("Dni[entero] Edad[entero]: ");
    scanf("%d %d", &auxint[0], &auxint[1]);
    miPersona->dni = auxint[0];
    miPersona->edad = auxint[1];

    char nombreAux[300];
    char apellidoAux[300];
    char correoElectronicoAux[300];
    printf("Nombre Apellido CorreoElectrónico: ");
    scanf("%s %s %s", nombreAux, apellidoAux, correoElectronicoAux);
    // strcpy
    miPersona->nombre = copiarCadena(nombreAux);
    miPersona->apellido = copiarCadena(apellidoAux);
    miPersona->correoElectronico = copiarCadena(correoElectronicoAux);
}

/**
 * @brief               Le asigna el valor del nuevo dni a la persona
 *                      pasada como parámetro.
 *
 * @param miPersona:    struct persona.
 *
 * @param nuevoDni:     nuevo dni de la persona.
 */
void establecerDni(Persona *miPersona, int nuevoDni)
{
    miPersona->dni = nuevoDni;
}

/**
 * @brief               Le asigna el valor del la nueva edad a la persona
 *                      pasada como parámetro.
 *
 * @param miPersona:    struct persona.
 *
 * @param nuevaEdad:     nueva edad de la persona.
 */
void establecerEdad(Persona *miPersona, int nuevaEdad)
{
    miPersona->edad = nuevaEdad;
}

/**
 * @brief               Le asigna el valor del nuevo nombre a la persona
 *                      pasada como parámetro.
 *
 * @param miPersona:    struct persona.
 *
 * @param nuevoNombre:     nuevo nombre de la persona.
 */
void establecerNombre(Persona *miPersona, char *nuevoNombre)
{
    miPersona->nombre = copiarCadena(nuevoNombre);
}

/**
 * @brief               Le asigna el valor del nuevo apellido a la persona
 *                      pasada como parámetro.
 *
 * @param miPersona:    struct persona.
 *
 * @param nuevoApellido:     nuevo apellido de la persona.
 */
void establecerApellido(Persona *miPersona, char *nuevoApellido)
{
    miPersona->apellido = copiarCadena(nuevoApellido);
}

/**
 * @brief               Le asigna el valor del nuevo correo electronico a la persona
 *                      pasada como parámetro.
 *
 * @param miPersona:    struct persona.
 *
 * @param nuevoCorreoElectronico:     nuevo correo electrónico de la persona.
 */
void establecerCorreoElectronico(Persona *miPersona, char *nuevoCorreoElectronico)
{
    miPersona->correoElectronico = copiarCadena(nuevoCorreoElectronico);
}

/**
 * @brief               Muestra en consola miPersona pasada como parámetro.
 *
 * @param miPersona     La direccion de memoria de la persona que quiero imprimir.
 */
void imprimirPersona(Persona *miPersona)
{
    printf("\nPersona");
    printf("[Dni = %d , ", miPersona->dni);
    printf("Edad = %d , ", miPersona->edad);
    printf("Nombre = %s , ", miPersona->nombre);
    printf("Apellido = %s , ", miPersona->apellido);
    printf("Correo Electronico = %s ]", miPersona->correoElectronico);
}
