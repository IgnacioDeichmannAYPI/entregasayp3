# Interpretar qué sucede a nivel memoria cuando llamamos
# a una función y le pasamos el struct.
-------------------------------------------------------

C acepta parámetros de tipo struct, y los trata como tipos primitivos, (char, int, float), es decir, que responde realizando una copia del registro del parámetro en memoria. 

Si no modificamos el valor del struct pasado como
parametro y no lo retornamos: el valor se perderá.

-------------------------------------------------------

Cuando llamamos a una función buscando modificar
los datos del struct sin retornar,debemos pasarle
la direccion de memoria de la variable tipo struct en
la que almacenamos una entidad que a la queremos modificarle
sus parametro, recordando pasarle el tipo de dato que 
le corresponde.
Con el operador &, podemos obtener la direccion de memoria del struct, y podrá recibirlo como parámetro de la función con el operador *


Pués C es un lenguaje que utiliza punteros de memoria, y también
copia de las referencias.

Cuando se declara una variable, en memoria se reseva el espacio máximo que puede ocupar un dato de dicho tipo, como peso en bytes de la variable.
El struct es un conjunto de variables concatenada.
Por lo tanto, cada struct tendrá como peso, la suma del tamaño
de sus variables declaradas en memoria.

Si no inicializamos el struct, con el literal NULL,
aquellas variables sin datos asignados, apuntaran a "basura", es decir direcciones de memoria con cualquier dato.

-------------------------------------------------------
 
 * Las funciones, se almacenan por separado en la memoria. Y se utilizan en conjunto del programa como si fueran datos.

 * Las cadenas de texto, char* y char[], apuntan a 
 direcciones de memoria.

 * Al usar punteros, hay que tomar la precacución de que 
 por error no se compartan referencias indeseadas.

 * Una solución, puede ser: copiar los datos de manera inmutable, utilizar los punteros con diferentes variables.

 * Mientras que si pasamos matrices o vectores como parámetros de una función, si se modifica la variable que pasamos desde el main.
 