#include <stdio.h>
/**Recibe nombre y edad de una persona y los imprime por pantalla.*/
int main()
{
    char nombreDeUnaPersona[100];
    int edadDeUnaPersona;

    printf("Ingrese el nombre de la persona: ");
    scanf("%s", nombreDeUnaPersona); // Internamente pasa el puntero de la primera posicion del arreglo de caracteres.

    printf("Ingrese la edad de %s: ", nombreDeUnaPersona);
    scanf("%d", &edadDeUnaPersona); // & obtiene la direccion de memoria de la variable de tipo entero

    /**
    * Podría imprimir la descripción de la persona:
    *
    printf("Persona[nombre=%s, edad:%d]", nombreDeUnaPersona, edadDeUnaPersona);
    */

    printf("%s ", nombreDeUnaPersona);
    printf("%d", edadDeUnaPersona);
    return 0;
}