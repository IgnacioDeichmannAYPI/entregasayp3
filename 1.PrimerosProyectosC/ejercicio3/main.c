#include <stdio.h>
/**A partir de un listado de números, determinar el mínimo.*/
int main()
{
    int numeros[] = {-77, 1, 2, 93, 3, 4, 78, -5, 6, 100, 7, 8, 9, 10};
    int numeroMinimo = numeros[0];
    int numerosLen = sizeof(numeros) / sizeof(int); // Len(Arreglo) = Peso(Arreglo)[bytes] / Peso(TipoDeDato)[bytes]
    for (int i = 0; i < numerosLen; i++)
        if (numeros[i] < numeroMinimo)
            numeroMinimo = numeros[i];
    printf("El numero mínimo es: %d", numeroMinimo);
    return 0;
}