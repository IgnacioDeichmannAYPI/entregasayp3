#include <stdio.h>
/** Determinar si un numero ingresado es par o impar*/
int main()
{
    int ingresado;
    printf("Ingrese un número: ");
    scanf("%d", &ingresado);
    if (ingresado % 2 == 0)
    {
        printf("%d es par.", ingresado);
    }
    else
    {
        printf("%d es impar.", ingresado);
    }

    return 0;
}