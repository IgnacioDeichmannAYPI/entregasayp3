#include <stdio.h>
/**A partir de un listado de números, determinar el máximo.*/
int main()
{
    int numeros[] = {77, 1, 2, 239, 9900, 3, -76, 43, 4, 78, 5, 6, 130, 7, 8, 9, 10};
    int numeroMaximo = numeros[0];

    int numerosLen = sizeof(numeros) / sizeof(int); // Len(Arreglo) = Peso(Arreglo)[bytes] / Peso(TipoDeDato)[bytes]
    for (int i = 0; i < numerosLen; i++)
        if (numeros[i] > numeroMaximo)
            numeroMaximo = numeros[i];

    printf("El numero máximo es: %d", numeroMaximo);

    return 0;
}