#include <stdio.h>
/**
Diseñar un menú navegable donde cada opción muestre una frase
distinta. Debe retornar al menú a menos que se elija la opción “Salir”.
*/
int main()
{
    printf("Menú navegable\n\t1.Opción 1\n\t2.Opción 2\n\t3.Opción 3\n\t4.Salir");
    int funciona = 1;
    int opcion;
    while (funciona)
    {
        printf("\nIngrese opción: ");
        scanf("%d", &opcion);

        if (opcion == 1)
            printf("Opción 1");
        if (opcion == 2)
            printf("Opción 2");
        if (opcion == 3)
            printf("Opción 3");
        if (opcion == 4)
            funciona = 0;
    }

    return 0;
}